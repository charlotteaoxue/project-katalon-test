<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Login test</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>20cb5603-fb63-4a4c-bf8d-bfc4a4718558</testSuiteGuid>
   <testCaseLink>
      <guid>e4dde6d4-17c2-4ac7-ac3c-bfa63cfbfd1b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Username_Require_test</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>15acafac-5474-4800-8dc5-2db665bc9b24</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Password_Require_test</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1c7bb780-9a1d-4db7-94fa-8827129caa35</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Incorrect_Username_Password</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
